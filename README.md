## Cnidaria assemblies

This repository contains the assemblies we generated for the analyses presented in:

> Zapata F, Goetz FE, Smith SA, Howison M, Siebert S, Church SH, Sanders SM, Ames CL, McFadden CS
> France SC, Daly M, Collins AG, Haddock SHD, Dunn CW, Cartwright P. (2015) Phylogenomic analyses support 
> traditional relationships within Cnidaria. PLoS One 10(10): e0139068. [doi:10.1371/journal.pone.0139068](http://dx.doi.org/10.1371/journal.pone.0139068). bioRxiv preprint [doi:10.1101/017632](http://dx.doi.org/10.1101/017632).

For analysis code, please visit the paper repository at [https://bitbucket.org/caseywdunn/cnidaria2014](https://bitbucket.org/caseywdunn/cnidaria2014)

The species included in this repository are:

* *Abylopsis tetragona*
* *Aegina citrea*
* *Agalma elegans*
* *Aiptasia pallida*
* *Alatina alata*
* *Anthomastus* sp.
* *Antipathes griggi*
* *Atolla vanhoeffeni*
* *Aurelia aurita*
* *Calibelemnon francei*
* *Candelabrum* sp.
* Cerianthid
* *Craseoa lathetica*
* *Craspedacusta* sp.
* *Ectopleuralarynx*
* *Haliclystus sanjuanensis*
* *Halitrephes valdiviae*
* *Hydractinia symbiolongicarpus*
* Keratoisidinae
* *Nanomia bijuga*
* *Nephthyigorgia* sp.
* *Obelia longissima*
* *Physalis physalis*
* *Podocoryna carnea*
* *Scleronephthya* sp.

Assemblies for all other species are publicly available elsewhere (NCBI-EST database, JGI, NHGRI, or FlyBase).

All data are in `Cnidaria_Transcriptomes` directory.